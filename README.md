INSTALL
=======
* `cp settings.sample.py settings.py`
* edit `settings.py` to reflect environment settings
* `source env/bin/activate` to activate virtual environment
* `python manage.py initdb`
* `python manage.py runserver`


MESSAGE DISPATCHER (API Server)
==============================

- An api-centric app whose sole purpose is to receive messages and dispatch them
- Client apps connect to send messages
- interfaces with the SMSC
- message queuing system



APP (Client)
============

- A component that initiates request to send messages 
- This can be an application developed by me or a third party who wishes to use the API server
- can be any arbitrary app eg. a bulk messaging app, mobile app, desktop app etc. that requires a gateway to use to send SMS.
- Only sends a message to a specified endpoint for dispatch to its destination

Pros
-----

- can send 1000s of messages without worrying about pooling, queuing, etc.

Deployment Models
-----------------

- SaaS ( available to the pulic for their bulk messaging needs, login is via an interface developed by BmaaS )
- Enterprise (with custom branding, feature development and dedicated support. Optional custom domain name)


Module
-------

 - users (user auth,)
 - dashboard (analytics, profiling)
 - messages (bulk, group messaging, )
 - resellers
 - services (birthday, reminders, api, )
