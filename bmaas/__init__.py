from flask import Flask, render_template

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager

from flask_debugtoolbar import DebugToolbarExtension


## Countries where services are operational
countries = [('GH', 'Ghana'), ('SEN', 'Senegal')]

app = Flask(__name__)
sa = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

# load in your app configuration file from settings.py
app.config.from_pyfile('../settings.py')

# register bebug toolbar with app. Use only in development
toolbar = DebugToolbarExtension(app)

from bmaas.modules.users.views import blueprint as UsersModule
from bmaas.modules.public.views import blueprint as PublicModule
from bmaas.modules.dashboard.views import blueprint as DashboardModule
# from bmaas.modules.messages.views import blueprint as MessagesModule
# from bmaas.modules.resellers.views import blueprint as ResellersModule

# register blueprints
app.register_blueprint(UsersModule, url_prefix='/users')
app.register_blueprint(PublicModule, url_prefix='/public')
app.register_blueprint(DashboardModule, url_prefix='/dashboard')
# app.register_blueprint(MessagesModule, url_prefix='/messages')
# app.register_blueprint(ResellersModule, url_prefix='/resellers')

@app.errorhandler(404)
def page_not_found(error):
    return render_template('errors/404.html')
