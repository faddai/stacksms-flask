from flask import redirect, url_for, abort, Blueprint, render_template

blueprint = Blueprint('dashboard', __name__)

def current_user():
    return None

@auth.login_required
def index():
    return render_template('dashboard/index.html', user=current_user())

@auth.login_required
def profile():
    return render_template('dashboard/profile.html', user=current_user())

@auth.login_required
def settings():
    return "settings page"


blueprint.add_url_rule('/', 'index', index)
blueprint.add_url_rule('/profile', 'profile', profile)
blueprint.add_url_rule('/settings', 'settings', settings)