"""
Module to provide plug-and-play authentication support for SQLAlchemy.
"""

import datetime
from sqlalchemy import Column, Integer, String, DateTime

from flask.ext.login import UserMixin

def get_user_class():
    """
    Factory function to create an SQLAlchemy User model with a declarative 
    base (for example db.Model from the Flask-SQLAlchemy extension).
    """
    from bmaas import sa
    class User(sa.Model, UserMixin):
        """
        Implementation of User for SQLAlchemy.
        """
        __tablename__ = 'users'
        extend_existing=True

        id = Column(Integer, primary_key=True)
        firstname = Column(String(20))
        lastname = Column(String(20))
        phone = Column(String(20))
        email = Column(String)
        country = Column(String)
        gender = Column(String)
        username = Column(String(80), unique=True, nullable=False)
        password = Column(String(120), nullable=False)
        salt = Column(String(80))
        role = Column(String(80))
        created = Column(DateTime(), default=datetime.datetime.utcnow)
        modified = Column(DateTime())

        def __init__(self, *args, **kwargs):
            super(User, self).__init__(*args, **kwargs)
        #     password = kwargs.get('password')
        #     if password is not None and not self.id:
        #         self.created = datetime.datetime.utcnow()
        #         # Initialize and encrypt password before first save.
        #         self.set_and_encrypt_password(password)

        def __getstate__(self):
            return {
                'id': self.id,
                'username': self.username,
                'role': self.role,
                'created': self.created,
                'modified': self.modified,
                'firstname': self.firstname,
                'lastname': self.lastname,
                'phone': self.phone,
                'country': self.country,
                'gender': self.gender,
                'email': self.email
            }

        def __repr__(self):
            return "<User %r %r>" % (self.firstname, self.lastname)

    return User
