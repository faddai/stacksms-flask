from flask.ext.wtf import Form, TextField, SelectField, PasswordField
from wtforms.validators import Email, Required, Length, EqualTo

countries = [('GH', 'Ghana'), ('SEN', 'Senegal')]

class SignupForm(Form):
    gender = [('male', 'Male'), ('female', 'Female')]

    firstname = TextField(u'First Name:', [Length(min=2, max=20)])
    lastname = TextField(u'Last Name:', [Length(min=2, max=20)])
    email = TextField(u'Email Address:', [Required(), Email()])
    phone = TextField(u'Phone Number:', [Length(min=9, max=13)])
    gender = SelectField(u'Gender:', [Required()], choices=gender)
    country = SelectField(u'Country:', [Required()], choices=countries)
    username = TextField(u'Username:', [Length(min=1, max=20)])
    password = PasswordField(u'Password:', 
    						[EqualTo('confirm_password', 
    						message='Password entered do not match'),
    						Required()])

    confirm_password = PasswordField(u'Confirm Password:', [Required()])

class LoginForm(Form):
	username = TextField(u'Username:', [Required()])
	password = PasswordField(u'Password:', [Required()])
	