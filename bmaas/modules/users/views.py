from flask import abort, redirect, url_for, request, session, flash
from flask import Blueprint, render_template

from flask.ext.login import (login_user, login_required, logout_user, 
                            current_user) 

import bmaas
from bmaas.modules.users.models import get_user_class
from bmaas.modules.users.forms import SignupForm, LoginForm

blueprint = Blueprint('users', __name__)

@blueprint.route("/logout")
@login_required
def logout():
    logout_user()
    flash('U have been successfully logged out')
    return redirect(url_for('public.index'))

@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    """
    Login form
    """
    User = get_user_class()

    form = LoginForm(request.form)
    # make sure data are valid, but doesn't validate password is right
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None:            
            if user.authenticate(form.password.data):
                flash('Welcome %s' % user.firstname)
                return redirect(url_for('dashboard.index'))
            else:
                flash('Wrong username or password', 'error-message')
        else:
            flash('Wrong username or password', 'error-message')

    return render_template("users/login.html", form=form)


@blueprint.route('/signup', methods=['GET', 'POST'])
def signup():
    """
    Signup Form
    """
    User = get_user_class()
    form = SignupForm(request.form)
    if form.validate_on_submit():

        # let's check if a user with this username exists
        u = User.query.filter_by(username=form.username.data).first()

        if u is not None:
            # I hate user data duplication, username has been taken up
            flash(u'The username "{}" has already been taken.'.format(form.username.data), 'error-message')
            return render_template("users/signup.html", form=form)
        # create an user instance not yet stored in the database
        user = User(
            username=form.username.data, 
            password=form.password.data,
            email=form.email.data,
            firstname=form.firstname.data,
            lastname=form.lastname.data,
            gender=form.gender.data,
            phone=form.phone.data,
            country=form.country.data)

        # Insert the record in our database and commit it
        bmaas.sa.session.add(user)
        bmaas.sa.session.commit()

        # flash will display a message to the user
        flash('Thanks for signing up!')
        # redirect user to the 'home' method of the user module.
        return redirect(url_for('dashboard.index'))
    return render_template("users/signup.html", form=form)
