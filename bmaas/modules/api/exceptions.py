class SMSGatewayError(Exception):

    def __init__(self, error_code, details):
        self.code = error_code
        self.details = details

    def __repr__(self):
        return "<SMSGatewayError: {} {}>".format(self.error_code, self.details)

    def __str__(self):
        return "<SMSGatewayError: {}>".format(self.details)

