routesms_responses = {
    "1025": "Insufficient Credit",
    "1701": "OK",
    "1702": "Invalid URL Error, This means that one of the parameters was not provided or left blank",
    "1703": "Invalid value in username or password field",
    "1704": "Invalid value in 'type' field",
    "1705": "Invalid Message",
    "1706": "Invalid Destination",
    "1707": "Invalid Source (Sender)",
    "1708": "Invalid value for 'dlr' field",
    "1709": "User validation failed",
    "1710": "Internal Error",
    "1715": "Response timeout"
}
