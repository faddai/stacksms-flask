import requests as r

from bmaas.modules.api.constants import routesms_responses
from bmaas.modules.api.exceptions import SMSGatewayError

class RouteSMS(object):
    """Connecting to RouteSMS"""
    def __init__(self, **args):
        self.username = args.get('username', "stacksms")
        self.password = args.get('password', "genP6zV4KbK1")

    def send(self, **args):
        """Send a message to the SMSC
        Returns a response formatted as code|destination_number|message_id
        """        
        message = args.get('message', '')
        to = args.get('to', '')
        sender_id = args.get('sender_id', "stackSMS")
        delivery_report = args.get('delivery_report', 1)
        type = args.get('type', 0)
        
        base_url = "http://smsplus1.routesms.com:8080/bulksms/bulksms" # no trailing slash, plsssss
        
        data = {
            'username': self.username,
            'password': self.password,
            'destination': to,
            'source': sender_id,
            'message': message,
            'dlr': delivery_report,
            'type': type
        }
        
        try:
            response = r.get(base_url, params=data).text.encode()
            response = response.split("|")

            if response[0] == '1701':
                return response[2]
            else:
                response_message = routesms_responses.get(response[0], None)
                raise SMSGatewayError(response[0], "{}: {}".format(response[0], response_message))
        except:
            print "Uncatched exception, get to work!" # hmmm, this shouldn't happen. If it does, hold RouteSMS accountable
            raise
