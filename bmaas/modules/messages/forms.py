from flask.ext.wtf import Form, TextField, DateTimeField, BooleanField, Required, Length

class MessageComposeForm(Form):
    """Compose a message"""
    
    to = TextField(u'Recipient', [Required()])
    body = TextField(u'Message', [Required()])
    sender_id = TextField(u'Sender ID', [Required(), Length(max=11)])
    #datetime = DateTimeField(u'Scheduled Date and Time')#, [Required()])
    save_message = BooleanField(u'Save a copy of Message', default=True)