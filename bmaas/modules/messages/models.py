import datetime as dt

from sqlalchemy import Column, Integer, String, DateTime, Text, Boolean

import bmaas import sa

class Message(sa.Model):
    """Messages sent through the service"""

    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    username = Column(String(20))
    to = Column(String(20), nullable=False)
    # text to account for multi 160 chars SMS
    body = Column(Text, nullable=False) 
    length = Column(Integer)
    # date and time message needs to be sent or will be 
    # sent in case of type SCHEDULED
    datetime = Column(DateTime()) 
    status = Column(String(10))
    sender_id_used = Column(String(11))
    type = Column(String(10))
    sent_via = Column(String) # medium through which message was sent (web, api)
    message_id = Column(String)
    save_message = Column(Integer)
    created = Column(DateTime())
    modified = Column(DateTime())

    def __init__(self, 
                 username=u'',
                 to=u'',
                 body=u'',
                 length=0,
                 datetime=None,
                 status=u'',
                 sender_id_used=u'',
                 type=u'text',
                 sent_via=u'web',
                 message_id=u'',
                 save_message=1):
        
        self.username = username
        self.to = to
        self.body = body
        self.length = length
        self.datetime = datetime
        self.status = status
        self.sender_id_used = sender_id_used
        self.type = type
        self.sent_via = sent_via
        self.save_message = save_message
        self.created = dt.datetime.utcnow()
        self.message_id = message_id
        
    def __repr__(self):
        return "<Message %r %r - %r>" % (self.username, self.to, self.datetime)

    def __str__(self):
        return "<Message %s->%s at %s>" % (self.username, self.to, self.datetime)