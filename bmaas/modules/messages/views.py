from flask import redirect, url_for, request, flash, session, Blueprint, render_template

# from bmaas import db
from bmaas.modules.messages.models import Message
from bmaas.modules.messages.forms import MessageComposeForm
from bmaas.modules.api.smsc import RouteSMS

blueprint = Blueprint('messages', __name__)
routesms = RouteSMS()

@auth.login_required
def all_messages():
    
    user = session.get('auth_user', None).get('username', '') # current user
    messages = Message.query.filter_by(username=user).all()
    return render_template('messages/view.html', messages=messages)

@auth.login_required   
def create_message():
    form = MessageComposeForm(request.form)

    if form.validate_on_submit():
        """Message has been validated. Continue processing and dispatch it as a task"""
        to = form.to.data
        sender = session.get('auth_user', None).get('username','')
        datetime = None
        body = form.body.data
        length = len(body)
        sender_id = form.sender_id.data
        save_message = form.save_message.data
        message_id = u''
        status = u''
        
        try:
            # a successful request would return a message reference id from the gateway
            message_id = routesms.send(message=body, to=to, sender_id=sender_id).text

        except SMSGatewayError as e:
            status = e.message

        message = Message(
                          username=sender,
                          to=to,
                          datetime=datetime,
                          body=body,
                          length=length,
                          sender_id_used=sender_id,
                          save_message=save_message,
                          message_id=message_id,
                          status=status)
        db.session.add(message)
        db.session.commit()

        flash('Messege sent. A copy has been saved for your messages history.', 'info')
        return redirect(url_for('.all_messages'))
    return render_template('messages/create.html', form=form)

@auth.login_required
def view_message(message_id):
    message = Message.query.get_or_404(message_id)

    if message is not None:
        return "Hello"
    else:
        pass

    render_template('messages/view.html', message=message)

"""
Delete message actually doesn't delete the message, :)
It only trashes it. It flags the message so that it doesn't
show up in UI. @TODO periodically purge data to get rid of
messages flagged.
"""
@auth.login_required
def delete_message(message_id):
    message = Message.query.get_or_404(message_id)
    message.trashed = 1
    db.session.commit()
    flash('Message has been Deleted')
    return redirect(url_for('.all_messages'))

blueprint.add_url_rule('/', 'all_messages', all_messages)
blueprint.add_url_rule('/create', 'create_message', create_message, methods=['GET', 'POST'])
blueprint.add_url_rule('/view/<int:message_id>', 'view_message', view_message)
blueprint.add_url_rule('/delete/<int:message_id>/', 'delete_message', delete_message, methods=['GET'])

