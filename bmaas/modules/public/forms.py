from flask.ext.wtf import Form, TextField, SelectField, TextAreaField
from wtforms.validators import Required, Length

class ContactForm(Form):
	name = TextField(u'Full Name', [Required(), Length(min=5, max=80)])
	email = TextField(u'Email Address', [Required()])
	phone = TextField(u'Phone Number', [Length(min=9, max=15)])
	subject = TextField(u'Subject', [Required(), Length(min=5, max=80)])
	message = TextAreaField(u'Message', [Required(), Length(min=5, max=80)])