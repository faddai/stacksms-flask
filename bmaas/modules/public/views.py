# -*- coding: utf-8 -*-
from flask import request, url_for, redirect, Blueprint, render_template
from flask.ext.wtf import Form, TextField, validators

from bmaas.modules.public.forms import ContactForm 

blueprint = Blueprint('public', __name__)

@blueprint.route('')
def start():
	return redirect(url_for('.index'))


@blueprint.route('/')
def index():
    return render_template('public/index.html')


@blueprint.route('/about')
def about_us():
    return render_template('public/about.html')


@blueprint.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm(request.form)
    if form.validate_on_submit():
        # shoot me an email
        return redirect(url_for('.index'))
    return render_template('public/contact.html', form=form)
