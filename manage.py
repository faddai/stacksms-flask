from flask.ext.script import Manager

import bmaas

manager = Manager(bmaas.app)

@manager.command
def initdb():
    """Creates all database tables."""
    bmaas.sa.create_all()


@manager.command
def dropdb():
    """Drops all database tables."""
    bmaas.sa.drop_all()

@manager.command
def prdb():
    """Print db object information"""
    print bmaas.sa

@manager.command
def clean():
    """Drop and create database and tables"""
    dropdb()
    initdb()

if __name__ == '__main__':
    manager.run()